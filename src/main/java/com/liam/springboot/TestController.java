package com.liam.springboot;

import com.liam.springboot.mysql.model.User;
import com.liam.springboot.mysql.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
public class TestController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping
    public String test(@Valid @RequestBody User user) {
        System.out.println(user);
        System.out.println("master");
        return "hello";
    }

    @PostMapping("/master")
    public String master(@Valid @RequestBody User user) {
        System.out.println(user);
        System.out.println("master");
        System.out.println("master3");
        System.out.println("master22");
        return "hello";
    }

    @GetMapping
    public List<User> findAll() {
        System.out.println("master1");
        System.out.println("master11");
        return userRepository.findAll();
    }

}
