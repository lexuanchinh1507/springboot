package com.liam.springboot.postgre.repository;

import com.liam.springboot.postgre.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Long> {
}
