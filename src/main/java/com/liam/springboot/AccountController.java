package com.liam.springboot;

import com.liam.springboot.postgre.model.Account;
import com.liam.springboot.postgre.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping
    public List<Account> findAll() {
        System.out.println("master2");
        System.out.println("master33");
        return accountRepository.findAll();
    }
}
