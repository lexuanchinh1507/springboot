package com.liam.springboot.mysql.repository;

import com.liam.springboot.mysql.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

}
